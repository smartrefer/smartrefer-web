import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetReferLogService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async select(refer_no:any) {
    const _url = `${this.apiUrl}/referog/select/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async onSave(datas:any) {
    const _url = `${this.apiUrl}/referog/insert`;
    return this.httpClient.post(_url,datas,this.httpOptions).toPromise();
  } 

}
