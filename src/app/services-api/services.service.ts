import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    // console.log(this.token);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async department() {
    const _url = `${this.apiUrl}/services/department`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 
  
  async appoint(hn:any,app_date:any) {
    const _url = `${this.apiUrl}/services/appoint/${hn}/${app_date}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async referout(start_date:any,end_date:any) {
    
    const _url = `${this.apiUrl}/services/referout/${start_date}/${end_date}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async referback(start_date:any,end_date:any) {
    // console.log(start_date);
    const _url = `${this.apiUrl}/services/referback/${start_date}/${end_date}`;
    // console.log(_url);
    
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async view(hn:any,seq:any,referno:any) {
    const _url = `${this.apiUrl}/services/view/${hn}/${seq}/${referno}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 

  async patient(cid:any) {
    const _url = `${this.apiUrl}/services/patient/${cid}`;
    console.log(_url);
    console.log(this.httpOptions);
    
    
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  } 


}