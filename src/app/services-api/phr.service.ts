import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhrService {
  token: any;
  apiUrlPhr:any;
  httpOptions: any;
  httpOptions2: any;

  constructor(private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    this.apiUrlPhr = sessionStorage.getItem('apiUrlPhr');
    //  console.log(this.apiUrlPhr);

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.token
      })
    };
    this.httpOptions2 = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJ1Ym9ucHJvbXB0IiwiaWF0IjoxNjYyMTIxMTY5LCJleHAiOjE2OTM2Nzg3Njl9.6ed1H86KOHCmJ0qu-3uKUceESq7RQ0JZBAKzPTmtTVs' 
      })
    };
  }

  async search(cid:any) {
    const _url = `${this.apiUrlPhr}/phr/search?cid=${cid}`;
    return this.httpClient.post(_url,this.httpOptions).toPromise();
  }   
  
  async viewsVisit(hospcode:any,hn:any,provider:any) {
    const _url = `${this.apiUrlPhr}/phr_visit/viewsVisit?hospcode=${hospcode}&hn=${hn}&provider=${provider}`;
    return this.httpClient.post(_url,this.httpOptions).toPromise();
  }   

  async viewsServices(cid:any,hospcode:any,vn:any,hn:any,provider:any) {
    const _url = `${this.apiUrlPhr}/phr_services/viewsServices?cid=${cid}&hospcode=${hospcode}&vn=${vn}&hn=${hn}&provider=${provider}`;
    return this.httpClient.post(_url,this.httpOptions).toPromise();
  }   
 
  async covidvaccine(cid:any) {
    const _url = `http://203.113.117.66/api/ubonprompt/ImmunizationTarget/person?cid=${cid}`;
    return this.httpClient.get(_url,this.httpOptions2).toPromise();
  }
}
