import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class KetAttachmentService {
  token: any;
  apiUrlSer:any;
  apiUrlReferboard:any;
  httpOptions: any;
  httpOptions2: any;

  constructor(@Inject('API_URL') private apiUrl: string,private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    this.apiUrlSer = sessionStorage.getItem('apiUrlSer');
    this.apiUrlReferboard = sessionStorage.getItem('apiUrlReferboard');
    // console.log(this.apiUrlSer);


    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
    this.httpOptions2 = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.token
      })
    };
  
  }

  async select(refer_no:any) {
    const _url = `${this.apiUrl}/attachment/select/${refer_no}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }  

  async delete(id:any){
    const _url = `${this.apiUrl}/attachment/delete?id=${id}`;
    return this.httpClient.delete(_url,this.httpOptions).toPromise();
  }

  async upload(file: File,refer_no:string,info:any){
    // console.log(file);
    const data = new FormData();
    data.append('file', file);
    // console.log(data);
    const _url = `${this.apiUrlSer}/uploads/${refer_no}/${info.att_type}/${info.hcode}`;
    // console.log(_url);
    
    return this.httpClient.post(_url,data,this.httpOptions2).toPromise();
  }

  link_upload(refer_no:any,att_name:any,mainhcode:any,tohcode:any){
    const _url = `${this.apiUrlReferboard}/uploads.php?refer_no=${refer_no}&att_name=${att_name}&mainhcode=${mainhcode}&tohcode=${tohcode}`;
    window.open(_url,"_blank");

  }

  async download(filename:any){
    // console.log(filename);
    const _url = `${this.apiUrlReferboard}/fileupload/${filename}`;
    // const _url = `${this.apiUrlSer}/uploads/${filename}`;
    window.open(_url,"_blank");
    // return this.httpClient.get(_url,this.httpOptions2).toPromise();
  }

}
