import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';

//lookup 
// import { GlobalVariablesService } from 'app/shared/globalVariables.service';
import { KetTypeptService } from 'app/services-api/ket-typept.service';
import { KetStrengthService } from 'app/services-api/ket-strength.service';
import { KetLoadsService } from 'app/services-api/ket-loads.service';
import { KetReferResultService } from 'app/services-api/ket-refer-result.service';
import { KetServiceplanService } from 'app/services-api/ket-serviceplan.service';
import { KetStationService } from 'app/services-api/ket-station.service';
import { VersionService } from 'app/services-api/version.service';
import { KetCauseReferbackService } from 'app/services-api/ket-cause-referback.service';
import { KetRefertriageService } from 'app/services-api/ket-refertriage.service';
import { KetRefertypeService } from 'app/services-api/ket-refertype.service';



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    scrHeight: any = 0;
    scrWidth: any = 0;

    itemeTypept: any = [];
    itemeStrength: any = [];
    itemeLoads: any = [];
    itemeResult: any = [];
    itemeServiceplan: any = [];
    itemeStation: any = [];

    itemeReferCause: any = [];
    itemeRefertriage: any = [];
    itemeRefertype: any = [];
    // menuMode = 'static';
    menuMode = 'overlay';

    // @HostListener('window:resize', ['$event'])
    // getScreenSize(event?: any) {
    //   this.scrHeight = (window.innerHeight);
    //   this.scrWidth = window.innerWidth;
    //   this.globalVariablesService.scrHeight = "" + this.scrHeight + "";
    //   this.globalVariablesService.scrWidth = "" + this.scrWidth + "";
    //   // console.log(this.scrHeight, this.scrWidth);
    // }

    constructor(
        // private globalVariablesService: GlobalVariablesService,
        private ketTypeptService: KetTypeptService,
        private ketStrengthService: KetStrengthService,
        private ketLoadsService: KetLoadsService,
        private ketReferResultService: KetReferResultService,
        private ketServiceplanService: KetServiceplanService,
        private ketStationService: KetStationService,
        private versionService: VersionService,
        private ketCauseReferbackService: KetCauseReferbackService,
        private ketRefertriageService: KetRefertriageService,
        private ketRefertypeService: KetRefertypeService,
        private router: Router,

    ) { }
    ngOnInit() {
        let token = sessionStorage.getItem('accessToken');

        // this.getScreenSize();
        console.log(token);
        
        if (token) {
            this.lookupDatas();
            // this.router.navigate(['/example']);
        }else{
            this.router.navigate(['/sign-in']);
        }

        // this.primengConfig.ripple = true;
        document.documentElement.style.fontSize = '14px';
    }


    lookupDatas() {
        this.getTypept();
        this.getStrength();
        this.getLoads();
        this.getResult();
        this.getServiceplan();
        this.getStation();
        this.referCause();
        this.refertriage();
        this.refertype();

        this.referServer();
        this.referServerPhr();
        this.referServerReferboard();
    }


    async getTypept() {
        try {
            let rs: any = await this.ketTypeptService.select();
            this.itemeTypept.push({ typept_id: '', typept_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeTypept.push(e);
            });
            // console.log('itemeTypept', this.itemeTypept);
            let i: any = JSON.stringify(this.itemeTypept);
            localStorage.setItem('itemeTypept', i);
        } catch (error) {
            console.log('itemeTypept', error);
        }
    }

    async getStrength() {
        try {
            let rs: any = await this.ketStrengthService.select();
            this.itemeStrength.push({ strength_id: '', strength_id_7: '', strength_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeStrength.push(e);
            });
            // console.log('itemeStrength', this.itemeStrength);
            let i: any = JSON.stringify(this.itemeStrength);
            localStorage.setItem('itemeStrength', i);
        } catch (error) {
            console.log('itemeStrength :', error);
        }
    }

    async getLoads() {
        try {
            let rs: any = await this.ketLoadsService.select();
            // console.log(rs);
            this.itemeLoads.push({ loads_id: '', loads_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeLoads.push(e);
            });
            // console.log('itemeLoads', this.itemeLoads);
            let i: any = JSON.stringify(this.itemeLoads);
            localStorage.setItem('itemeLoads', i);
        } catch (error) {
            console.log('itemeLoads ', error);
        }
    }


    async getResult() {
        try {
            let rs: any = await this.ketReferResultService.select();
            this.itemeResult.push({ refer_result_id: '', refer_result_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeResult.push(e);
            });
            // console.log('itemeResult', this.itemeResult);
            let i: any = JSON.stringify(this.itemeResult);
            localStorage.setItem('itemeResult', i);
        } catch (error) {
            console.log('itemeResult', error);

        }
    }

    async getServiceplan() {
        try {
            let rs: any = await this.ketServiceplanService.select();
            this.itemeServiceplan.push({ serviceplan_id: '', serviceplan_name: 'กรุณาเลือก', serviceplan_sortno: '' });
            rs.forEach((e: any) => {
                this.itemeServiceplan.push(e);
            });
            // console.log('itemeServiceplan', this.itemeServiceplan);
            let i: any = JSON.stringify(this.itemeServiceplan);
            localStorage.setItem('itemeServiceplan', i);
        } catch (error) {
            console.log('itemeServiceplan', error);

        }
    }

    async getStation() {
        try {
            let rs: any = await this.ketStationService.select();
            this.itemeStation.push({ station_id: '', station_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeStation.push(e);
            });
            // console.log('itemeStation', this.itemeStation);
            let i: any = JSON.stringify(this.itemeStation);
            localStorage.setItem('itemeStation', i);
        } catch (error) {
            console.log('itemeStation', error);

        }
    }

    async referCause() {
        try {
            let rs: any = await this.ketCauseReferbackService.select();
            this.itemeReferCause.push({ cause_referback_id: '', cause_referback_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeReferCause.push(e);
            });
            // console.log('itemeReferCause',this.itemeReferCause);
            let i: any = JSON.stringify(this.itemeReferCause);
            localStorage.setItem('itemeReferCause', i);

        } catch (error) {
            console.log('itemeReferCause', error);

        }
    }


    async refertriage() {
        try {
            let rs: any = await this.ketRefertriageService.select();
            this.itemeRefertriage.push({ id: '', name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeRefertriage.push(e);
            });
            // console.log('itemeRefertriage',this.itemeRefertriage);
            let i: any = JSON.stringify(this.itemeRefertriage);
            localStorage.setItem('itemeRefertriage', i);

        } catch (error) {
            console.log('itemeRefertriage', error);

        }
    }


    async refertype() {
        try {
            let rs: any = await this.ketRefertypeService.select();
            this.itemeRefertype.push({ refer_type: '', refer_type_name: 'กรุณาเลือก' });
            rs.forEach((e: any) => {
                this.itemeRefertype.push(e);
            });
            // console.log('itemeRefertype',this.itemeRefertype);
            let i: any = JSON.stringify(this.itemeRefertype);
            localStorage.setItem('itemeRefertype', i);

        } catch (error) {
            console.log('itemeRefertype', error);

        }
    }

    async referServer() {
        try {
            let rs: any = await this.versionService.referServer();
            let apiUrlSer = rs[0].api_url;
            // console.log("apiUrlSer",apiUrlSer);
            sessionStorage.setItem('apiUrlSer', apiUrlSer);

        } catch (error) {
            console.log('apiUrlSer', error);

        }
    }

    async referServerPhr() {
        try {
            let rs: any = await this.versionService.referSerferPhr();
            let apiUrlPhr = rs[0].api_url;
            //  console.log("apiUrlPhr",apiUrlPhr);
            sessionStorage.setItem('apiUrlPhr', apiUrlPhr);

        } catch (error) {
            console.log('apiUrlPhr', error);

        }
    }

    async referServerReferboard() {
        try {
            let rs: any = await this.versionService.selectReferboard();
            let apiUrlReferboard = rs[0].api_url;
            // console.log("apiUrlReferboard",apiUrlReferboard);
            sessionStorage.setItem('apiUrlReferboard', apiUrlReferboard);

        } catch (error) {
            console.log('apiUrlReferboard', error);

        }
    }
}
