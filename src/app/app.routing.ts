import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: 'example'},

    // Redirect signed in user to the '/example'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: 'example'},

    // Auth routes for guests
    {
        path: '',
        canActivate: [NoAuthGuard],
        canActivateChild: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'confirmation-required', loadChildren: () => import('app/modules/auth/confirmation-required/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule)},
            {path: 'forgot-password', loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)},
            {path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)},
            {path: 'sign-in', loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)},
            {path: 'sign-up', loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule)}
        ]
    },

    // Auth routes for authenticated users
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            {path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule)},
            {path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ]
    },

    // Landing routes referout
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'referout', loadChildren: () => import('app/modules/landing/referout/referout/referout.module').then(m => m.ReferoutModule)},
            {path: 'referout-create', loadChildren: () => import('app/modules/landing/referout/referout-create/referout-create.module').then(m => m.ReferoutCreateModule)},
            {path: 'referout-views', loadChildren: () => import('app/modules/landing/referout/referout-views/referout-views.module').then(m => m.ReferoutViewsModule)},
            {path: 'referout-views-only', loadChildren: () => import('app/modules/landing/referout/referout-views-only/referout-views-only.module').then(m => m.ReferoutViewsOnlyModule)},
        ]
    },

    // Landing routes referin
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'referin', loadChildren: () => import('app/modules/landing/referin/referin/referin.module').then(m => m.ReferinModule)},
            {path: 'referin-views', loadChildren: () => import('app/modules/landing/referin/referin-views/referin-views.module').then(m => m.ReferinViewsModule)},
        ]
    },

    // Landing routes referback
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'referback', loadChildren: () => import('app/modules/landing/referback/referback/referback.module').then(m => m.ReferbackModule)},
            {path: 'referback-create', loadChildren: () => import('app/modules/landing/referback/referback-create/referback-create.module').then(m => m.ReferbackCreateModule)},
            {path: 'referback-views', loadChildren: () => import('app/modules/landing/referback/referback-views/referback-views.module').then(m => m.ReferbackViewsModule)},
            {path: 'referback-views-only', loadChildren: () => import('app/modules/landing/referback/referback-views-only/referback-views-only.module').then(m => m.ReferbackViewsOnlyModule)},
        ]
    },

    // Landing routes referreceive
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'referreceive', loadChildren: () => import('app/modules/landing/referreceive/referreceive/referreceive.module').then(m => m.ReferreceiveModule)},
            {path: 'referreceive-views', loadChildren: () => import('app/modules/landing/referreceive/referreceive-views/referreceive-views.module').then(m => m.ReferreceiveViewsModule)},
        ]
    },

    // Landing routes report
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'imc', loadChildren: () => import('app/modules/landing/report/imc/imc/imc.module').then(m => m.ImcModule)},
            {path: 'imc-save', loadChildren: () => import('app/modules/landing/report/imc/imc-save/imc-save.module').then(m => m.ImcSaveModule)},
            {path: 'imc-views', loadChildren: () => import('app/modules/landing/report/imc/imc-views/imc-views.module').then(m => m.ImcViewsModule)},
            {path: 'phr', loadChildren: () => import('app/modules/landing/report/phr/phr.module').then(m => m.PhrModule)},
            {path: 'report', loadChildren: () => import('app/modules/landing/report/report/report.module').then(m => m.ReportModule)},
        ]
    },

    // Landing routes other
    {
        path: '',
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'appoint', loadChildren: () => import('app/modules/landing/other/appoint/appoint.module').then(m => m.AppointModule)},
            {path: 'assess', loadChildren: () => import('app/modules/landing/other/assess/assess/assess.module').then(m => m.AssessModule)},
            {path: 'assess-views', loadChildren: () => import('app/modules/landing/other/assess/assess-views/assess-views.module').then(m => m.AssessViewsModule)},
            {path: 'evaluate', loadChildren: () => import('app/modules/landing/other/evaluate/evaluate.module').then(m => m.EvaluateModule)},
            {path: 'telemed', loadChildren: () => import('app/modules/landing/other/telemed/telemed/telemed.module').then(m => m.TelemedModule)},
            {path: 'telemed-views', loadChildren: () => import('app/modules/landing/other/telemed/telemed-views/telemed-views.module').then(m => m.TelemedViewsModule)},
            {path: 'telemed-views-only', loadChildren: () => import('app/modules/landing/other/telemed/telemed-views-only/telemed-views-only.module').then(m => m.TelemedViewsOnlyModule)},
        ]
    },

    // Admin routes
    {
        path       : '',
        // canActivate: [AuthGuard],
        // canActivateChild: [AuthGuard],
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            {path: 'example', loadChildren: () => import('app/modules/admin/example/example.module').then(m => m.ExampleModule)},
        ]
    }
];
