import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';

import { LoginService } from 'app/services-api/login.service'
import {AlertService} from 'app/services/alert.service';

@Component({
    selector     : 'auth-sign-in',
    templateUrl  : './sign-in.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignInComponent implements OnInit
{
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signInForm: UntypedFormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router,
        private loginService: LoginService,
        private alertService: AlertService
    )
    { }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        let token = sessionStorage.getItem('accessToken');
        console.log(token);
        if (token) {
            // this._router.navigate(['/home']);
        }
        
        // Create the form
        this.signInForm = this._formBuilder.group({
            username     : ['', Validators.required],
            password  : ['', Validators.required]
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    async signIn(){
        // Return if the form is invalid
        if ( this.signInForm.invalid )
        {
            return;
        }

        // Disable the form
        // this.signInForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        let info: any = this.signInForm.value;
        try {
            let rs: any = await this.loginService.onLogin(info);
            //  console.log(rs);
      
            
            if (rs.ok) {
              // console.log(rs.payload.gateway_token);
              let gateway_token = rs.payload.gateway_token;
              let info = rs.payload.info[0];
              
              console.log(info);
              
              sessionStorage.setItem('accessToken', gateway_token);
              sessionStorage.setItem('token', gateway_token);
              sessionStorage.setItem('username', info.username);
              sessionStorage.setItem('fullname', info.fullname);
              sessionStorage.setItem('hcode', info.hcode);
    
              sessionStorage.getItem('hcode');
              console.log(sessionStorage.getItem('accessToken'));
              
            //   window.location.reload();
    
              this._router.navigate(['/referout']);
    
      
            } else {
              // ล๊อกอินไม่ถูก หรือ api ไม่ทำงาน
              this.alertService.error('ล๊อกอินไม่ถูก หรือ api ไม่ทำงาน','ข้อผิดพลาด');
              // console.log('No data');
            }
          } catch (error) {
            // console.log('loginService',error);
            // ออกเน็ตไม่ได้
            this.alertService.error('ออกเน็ตไม่ได้','ข้อผิดพลาด');
          }

    }
}
