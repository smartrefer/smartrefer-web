import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferreceiveComponent } from './referreceive.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referreceiveRoutes: Route[] = [
  {
      path     : '',
      component: ReferreceiveComponent
  }
];

@NgModule({
  declarations: [
    ReferreceiveComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referreceiveRoutes)

  ]
})
export class ReferreceiveModule { }
