import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferreceiveComponent } from './referreceive.component';

describe('ReferreceiveComponent', () => {
  let component: ReferreceiveComponent;
  let fixture: ComponentFixture<ReferreceiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferreceiveComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferreceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
