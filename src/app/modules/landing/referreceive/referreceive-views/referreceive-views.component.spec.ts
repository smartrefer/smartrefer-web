import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferreceiveViewsComponent } from './referreceive-views.component';

describe('ReferreceiveViewsComponent', () => {
  let component: ReferreceiveViewsComponent;
  let fixture: ComponentFixture<ReferreceiveViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferreceiveViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferreceiveViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
