import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferreceiveViewsComponent } from './referreceive-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referreceiveViewsRoutes: Route[] = [
  {
      path     : '',
      component: ReferreceiveViewsComponent
  }
];

@NgModule({
  declarations: [
    ReferreceiveViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referreceiveViewsRoutes)
  ]
})
export class ReferreceiveViewsModule { }
