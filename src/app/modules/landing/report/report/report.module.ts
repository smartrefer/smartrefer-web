import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportComponent } from './report.component';

import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const reportRoutes: Route[] = [
  {
      path     : '',
      component: ReportComponent
  }
];


@NgModule({
  declarations: [
    ReportComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(reportRoutes)
  ]
})
export class ReportModule { }
