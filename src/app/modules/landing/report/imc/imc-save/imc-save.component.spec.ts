import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImcSaveComponent } from './imc-save.component';

describe('ImcSaveComponent', () => {
  let component: ImcSaveComponent;
  let fixture: ComponentFixture<ImcSaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImcSaveComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImcSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
