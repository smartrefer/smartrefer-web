import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImcSaveComponent } from './imc-save.component';

import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const imcSaveRoutes: Route[] = [
  {
      path     : '',
      component: ImcSaveComponent
  }
];

@NgModule({
  declarations: [
    ImcSaveComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(imcSaveRoutes)
  ]
})
export class ImcSaveModule { }
