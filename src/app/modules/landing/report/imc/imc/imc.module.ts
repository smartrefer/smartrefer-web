import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImcComponent } from './imc.component';

import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const imcRoutes: Route[] = [
  {
      path     : '',
      component: ImcComponent
  }
];

@NgModule({
  declarations: [
    ImcComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(imcRoutes)
  ]
})
export class ImcModule { }
