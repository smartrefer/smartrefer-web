import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImcViewsComponent } from './imc-views.component';

import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const imcViewsRoutes: Route[] = [
  {
      path     : '',
      component: ImcViewsComponent
  }
];


@NgModule({
  declarations: [
    ImcViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(imcViewsRoutes)
  ]
})
export class ImcViewsModule { }
