import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImcViewsComponent } from './imc-views.component';

describe('ImcViewsComponent', () => {
  let component: ImcViewsComponent;
  let fixture: ComponentFixture<ImcViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImcViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImcViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
