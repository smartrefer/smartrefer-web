import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhrComponent } from './phr.component';

import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const phrRoutes: Route[] = [
  {
      path     : '',
      component: PhrComponent
  }
];


@NgModule({
  declarations: [
    PhrComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(phrRoutes)
  ]
})
export class PhrModule { }
