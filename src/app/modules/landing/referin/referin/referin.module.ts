import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ReferinComponent } from './referin.component';

const referinRoutes: Route[] = [
  {
      path     : '',
      component: ReferinComponent
  }
];

@NgModule({
  declarations: [
    ReferinComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referinRoutes)

  ]
})
export class ReferinModule { }
