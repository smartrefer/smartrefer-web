import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferinComponent } from './referin.component';

describe('ReferinComponent', () => {
  let component: ReferinComponent;
  let fixture: ComponentFixture<ReferinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
