import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferinViewsComponent } from './referin-views.component';

describe('ReferinViewsComponent', () => {
  let component: ReferinViewsComponent;
  let fixture: ComponentFixture<ReferinViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferinViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferinViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
