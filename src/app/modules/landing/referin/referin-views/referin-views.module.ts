import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferinViewsComponent } from './referin-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referinViewsRoutes: Route[] = [
  {
      path     : '',
      component: ReferinViewsComponent
  }
];


@NgModule({
  declarations: [
    ReferinViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referinViewsRoutes)

  ]
})
export class ReferinViewsModule { }
