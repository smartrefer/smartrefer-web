export interface ReferElement {
    strength_id: string;
    refer_triage_id: string;
    attpath_appoint: string;
    refer_appoint: string;
    location_refer_name: string;
    load_name: string;
    load_id: string;
    hn: string;
    fullname: string;
    cid: string;
    refer_no: string;
    refer_date: string;
    refer_time: string;
    refer_hospname?: string;
    receive_no?: string;
    receive_refer_result_id?: string;
    receive_spclty_name?: string;
    receive_date?: string;
    receive_time?: string;
    receive_result_name?: string;
  }