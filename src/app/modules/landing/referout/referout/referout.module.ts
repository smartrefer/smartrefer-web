import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferoutComponent } from 'app/modules/landing/referout/referout/referout.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referoutRoutes: Route[] = [
  {
      path     : '',
      component: ReferoutComponent
  }
];

@NgModule({
  declarations: [
    ReferoutComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referoutRoutes)

    
  ]
})
export class ReferoutModule { }
