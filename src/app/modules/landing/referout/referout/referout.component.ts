import { Component, OnInit, Input, ElementRef, NgZone, ViewChild } from '@angular/core';
import { KetReferoutService } from 'app/services-api/ket-referout.service';
import { GlobalVariablesService } from 'app/shared/globalVariables.service';
import { ServicesService } from 'app/services-api/services.service';
import { AlertService } from 'app/services/alert.service';
import { KetAttachmentService } from 'app/services-api/ket-attachment.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import * as moment from 'moment-timezone';
import { MqttClient } from 'mqtt';
import * as mqttClient from 'app/shared/vendor/mqtt';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

import { ReferElement } from 'app/modules/landing/referout/referout/referout';

@Component({
  selector: 'app-referout',
  templateUrl: './referout.component.html',
  styleUrls: ['./referout.component.scss']
})

export class ReferoutComponent implements OnInit {

  @ViewChild('pdfTable') pdfTable!: ElementRef;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  displayedColumns: string[] = ['id', 'file', 'appoint', 'clinic', 'load', 'hn', 'name', 'cid', 'refer-no', 'refer-date', 'refer-time', 'refer-hosp', 'refer-in-no', 'receive', 'receive-date', 'receive-time', 'cancel']
  dataSource: any;

  isOffline = false;
  client: MqttClient;
  notifyUser = null;
  notifyPassword = null;
  notifyUrl: string;

  start_out_date: any;
  end_out_date: any;

  scrHeight: number = 0;
  scrWidth: number = 0;
  boxHeight: any;
  rowsData: any[] = [];
  device: any = 't';

  // today: any = moment(Date()).tz('Asia/Bangkok').format('YYYY-MM-DD');
  today: Date = new Date();

  blockedPanel: boolean = false;  //สำหรับ ป้องกันหน้าจอตอนโปรเซสทำงานยังไม่เสร็จ

  selectedRowsData: any = [];

  statuses: any[] = [];

  loading: boolean = true;

  displayCreate: boolean = false;
  rowsDataCreate: any[] = [];
  loadingCreate: boolean = true;
  rowsDataTemp: any[] = [];
  rowsReportData: any = {};
  displayReferOut: boolean = false;
  rowsDataReferOut: any[] = [];
  loadingReferOut: boolean = true;

  // @ViewChild('dt') table!: Table;

  // @ViewChild('dtCreate') tableCreate!: Table;

  first: any = 0;

  rows: any = 10;

  hcode: any;

  sdate: Date;
  edate: Date;

  sdateCreate: any;
  edateCreate: any;

  limitReferOut: any = 10000;

  buttonClassStrength: string = 'p-button-rounded p-button-warning';
  value3: any;
  justifyOptions: any = [];
  value1: string = "off";
  stateOptions: any = [];

  ptableStyle: any = {
    width: '100%',
    // height: '100%',
    flex: '1 1 auto'
  };
  scrollHeight: string = '';

  validateForm: boolean = false;


  checked: boolean = true;
  activeIndex1: number = 0;
  cid: any;

  exportColumns: any = [];
  // paginator: boolean = true;
  colHnWidth: any = '0 0 80px';
  specTable: any;

  constructor(

    // private primengConfig: PrimeNGConfig,
    private ketReferoutService: KetReferoutService,
    private globalVariablesService: GlobalVariablesService,
    private servicesService: ServicesService,
    private router: Router,
    private alertService: AlertService,
    private ketAttachmentService: KetAttachmentService,
    // private initTableServcie: InitTableServcie,
    private zone: NgZone,


  ) {

    this.hcode = sessionStorage.getItem('hcode');
    this.sdate = new Date('2560-12-10');
    // this.sdate = this.today;
    this.edate = this.today;

    this.sdateCreate = this.today;
    this.edateCreate = this.today;
    this.cid = this.cid;
    console.log(window.innerWidth);
    // this.specTable = this.initTableServcie.specTable;
    console.log(this.specTable);

    // config mqtt
    this.notifyUrl = `ws://203.113.117.66:8080`;
    this.notifyUser = `q4u`;
    this.notifyPassword = `##q4u##`;

  }
  // Medthod  ที่ต้องเหมือนกันทุก component/////////////////////
  readGlobalValue() {
    this.scrHeight = Number(this.globalVariablesService.scrHeight);
    this.scrWidth = Number(this.globalVariablesService.scrWidth);
    this.boxHeight = ((this.scrHeight) - 80) + 'px';
    // console.log(this.scrHeight + ":" + this.scrWidth);
    this.scrollHeight = (Number(this.globalVariablesService.scrHeight) - 380) + 'px';
    // กำหนด ความสูง  ความกว้างของตาราง 
    this.ptableStyle = {
      // width: (this.scrWidth-20) + 'px',
      width: '100%',
      height: (this.scrHeight - 300) + 'px',
    }
  }

  // กำหนดค่าเกี่ยวกับ paginator  table 
  reset() {
    this.first = 0;
  }
  isLastPage(): boolean {
    return this.rowsData ? this.first === (this.rowsData.length - this.rows) : true;
  }
  isFirstPage(): boolean {
    let r = this.rowsData ? this.first === 0 : true;
    console.log(r);

    return this.rowsData ? this.first === 0 : true;
  }
  next() {
    this.first = this.first + this.rows;
  }
  prev() {
    this.first = this.first - this.rows;
  }
  ////////////////////////////////////////  

  ngOnInit() {
    this.readGlobalValue();
    this.getInfo();
    this.connectWebSocket();
    // this.primengConfig.ripple = true;
    if (sessionStorage.getItem('start_out_date') && sessionStorage.getItem('end_out_date')) {
      this.sdate = JSON.parse(sessionStorage.getItem('start_out_date'));
      this.edate = JSON.parse(sessionStorage.getItem('end_out_date'));
    }

  }

  onSearch() {
    console.log('On Search');
    sessionStorage.setItem('start_out_date', JSON.stringify(this.sdate));
    sessionStorage.setItem('end_out_date', JSON.stringify(this.edate));

    this.getInfo();
  }

  onClear() {
    this.sdate = this.today;
    this.edate = this.today;
    sessionStorage.setItem('start_out_date', JSON.stringify(this.sdate));
    sessionStorage.setItem('end_out_date', JSON.stringify(this.edate));
    // this.rowsData = [];
    // this.dataSource = [];

    this.getInfo();
  }

  onDateSelect(value: any) {
    // this.table.filter(this.formatDate(value), 'date', 'equals')
  }

  formatDate(date: any) {
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day;
  }

  async getInfo() {
    this.loading = true;
    this.rowsData = [];
    this.dataSource = [];

    // console.log("getInfo");
    // console.log("hcode", this.hcode);
    // console.log("sdate", this.sdate);
    // console.log("edate", this.edate);
    // console.log("limitReferOut", this.limitReferOut);
    let startDate: any;
    let endDate: any;
    // console.log(startDate);
    this.start_out_date = moment(JSON.parse(sessionStorage.getItem('start_out_date'))).format('YYYY-MM-DD');
    this.end_out_date =  moment(JSON.parse(sessionStorage.getItem('end_out_date'))).format('YYYY-MM-DD');
    console.log("start_out_date", this.start_out_date);
    console.log("end_out_date", this.end_out_date);

    if (sessionStorage.getItem('start_out_date') && sessionStorage.getItem('end_out_date')) {
      startDate = this.start_out_date;
      endDate = this.end_out_date;
    } else {
      startDate = this.sdate.getFullYear() + "-" + (this.sdate.getMonth() + 1) + "-" + this.sdate.getDate();
      endDate = this.edate.getFullYear() + "-" + (this.edate.getMonth() + 1) + "-" + this.edate.getDate();
    }

    let rs: any;
    try {
      if (this.cid) {
        rs = await this.ketReferoutService.selectCid(this.cid, this.hcode, 'OUT');
      } else {
        rs = await this.ketReferoutService.select(this.hcode, startDate, endDate, this.limitReferOut);
      }
      console.log(rs);
      let item: any = rs[0];
      // console.log(item);
      if (item) {
        console.log(rs);
        this.rowsData = await rs;
        console.log(this.rowsData);
        this.dataSource = new MatTableDataSource<ReferElement>(this.rowsData);
        this.dataSource.paginator = this.paginator;
        this.loading = false;
      } else {
        // console.log();
        this.loading = false;
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loading = false;
    }
  }

  async onCreate() {
    // console.log('On Create');
    // this.displayCreate = true;
    // this.getCreateReferOut();
    this.globalVariablesService.paramsChild = 'paramFrom ReferOut';
    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     "firstname": "Nic",
    //     "lastname": "Raboy"
    //   }
    // };

    this.router.navigate(['/referout-create']);

    sessionStorage.setItem('routmain', '/referout');

    // this.router.navigate(['/product-list'], { queryParams: { page: "pageNum" } });

  }

  async getCreateReferOut() {
    // console.log("get Create ReferOut");
    // console.log("sdateCreate", this.sdateCreate);
    // console.log("edateCreate", this.edateCreate);
    this.loadingCreate = true;
    try {
      let rs: any = await this.servicesService.referout(this.sdateCreate, this.edateCreate);
      // console.log(rs);
      let item: any = rs[0];
      if (item) {
        // console.log(item);
        this.rowsDataCreate = rs;
        // console.log(this.rowsDataCreate);
        this.loadingCreate = false;
      } else {
        // console.log();
        this.loadingCreate = false;
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loadingCreate = false;
    }
  }

  async onSearchCreate() {
    // console.log('On Search Create');

    this.justifyOptions = [
      { icon: 'pi pi-align-left', justify: 'Left' },
      { icon: 'pi pi-align-right', justify: 'Right' },
      { icon: 'pi pi-align-center', justify: 'Center' },
      { icon: 'pi pi-align-justify', justify: 'Justify' }
    ];
    this.stateOptions = [
      { label: "Off", value: "off" },
      { label: "On", value: "on" }
    ];
    this.getCreateReferOut();
  }
  async onReferOut(i: any) {
    // console.log('On ReferOut' , i);
    this.getReferOut(i);
    this.displayReferOut = true;
    this.displayCreate = false;

  }

  async getReferOut(i: any) {
    // console.log("get ReferOut");
    // console.log("hn", i.hn);
    // console.log("seq", i.seq);
    // console.log("referno", i.referno);
    this.loadingReferOut = true;
    try {
      let rs: any = await this.servicesService.view(i.hn, i.seq, i.referno,);
      // console.log(rs);
      let item: any = rs[0];
      if (item) {
        // console.log(item);
        this.rowsDataReferOut = rs;
        // console.log(this.rowsDataReferOut);
        this.loadingReferOut = false;
      } else {
        // console.log();
      }
    } catch (error) {
      console.log(error);
      // this.alertService.error();
      this.loadingReferOut = false;
    }
  }
  onDateSelected() {
    // console.log('lddl');
    if (this.edate < this.sdate) {
      this.alertService.error("ข้อผิดพลาด !", "วันเริ่มต้น มากกว่า วันสิ้นสุด ไม่ได้");
      this.validateForm = false;
    } else {
      this.validateForm = true;
    }
  }

  onRowSelect(event: any) {
    // console.log(event);
    // console.log(data);
    sessionStorage.setItem('itemStorage', JSON.stringify(event.data));
    // this.globalVariablesService.addNewList(i)
    this.router.navigate(['/referout-views-only']);
    sessionStorage.setItem('routmain', '/referout');

  }

  async onReferOutView(i: never) {
    // console.log('On ReferOutView', i);
    sessionStorage.setItem('itemStorage', JSON.stringify(i));
    // this.globalVariablesService.addNewList(i)
    this.router.navigate(['/referout-views-only']);
    sessionStorage.setItem('routmain', '/referout');
  }

  handleChange(e: any) {
    let isChecked = e.checked;
  }

  uploadsRoute(datas: any) {
    // console.log(datas);
    this.ketAttachmentService.link_upload(datas.refer_no, datas.fullname, this.hcode, datas.refer_hospcode);
    // let strdata:any = JSON.stringify(datas);
    // sessionStorage.setItem('strdata',strdata);
    // this.router.navigate(['/home/uploads']);
  }
  referOutViewsRoute() {
    // console.log('referOutViewsRoute')
    this.router.navigate(['/referout-views']);
    sessionStorage.setItem('routmain', '/referout');

  }
  async deleteReferOut(i: any) {
    // console.log('deleteReferOut : ',i);

    let confirm = await this.alertService.confirm("ต้องการลบรายการนี้ ใช่หรือไม่");
    // console.log(confirm);

    if (confirm) {
      let rs = await this.ketReferoutService.onDelete(i)
      // console.log(rs);
      this.getInfo();
    }

  }
  async downloadAttRoute(filename: any) {
    // console.log(filename);
    let download: any = await this.ketAttachmentService.download(filename);
  }

  onClickSssess(datas: any) {
    // console.log(datas);
    let Storage: any = JSON.stringify(datas);
    sessionStorage.setItem('itemStorage', Storage);
    this.router.navigate(['/assess-view']);
  }

  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.rowsData);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "referout");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  async exportexcel2() {
    // this.paginator = false;
    // this.rowsDataTemp = this.rowsData;
    // this.rowsData = [];
    // this.rowsData = this.reDraw();
    let checkT = await this.reDraw();
    if (checkT) {

      let fileName = 'ExcelSheet.xlsx';
      /* table id is passed over here */
      let element = document.getElementById('dt');
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

      /* generate workbook and add the worksheet */
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      /* save to file */
      XLSX.writeFile(wb, fileName);
    }
  }

  async reDraw() {
    // this.paginator = false;
    this.rowsDataTemp = this.rowsData;
    this.rowsData = [];
    this.rowsData = this.rowsDataTemp;
    return true;
  }

  async exportExcel3() {

    // this.rowsDataTemp = this.rowsData;
    let fileName = 'ExcelSheet.xlsx';
    /* table id is passed over here */
    let element = document.getElementById('dtTemp');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, fileName);

  }

  valuechange(event: any) {
    // console.log(event.target.value.length);
    if (event.target.value.length == 13) {
      this.validateForm = true;
    }
  }

  connectWebSocket() {
    // const rnd = new Random();
    const clientId = `smartrefer-${new Date().getTime()}`;

    try {
      this.client = mqttClient.connect(this.notifyUrl, {
        clientId: clientId,
        username: this.notifyUser,
        password: this.notifyPassword
      });
    } catch (error) {
      console.log(error);
    }

    const topic = `smartrefer/${this.hcode}`;

    const that = this;

    this.client.on('message', async (topic, payload) => {
      try {
        const _payload = JSON.parse(payload.toString());
        if (_payload.refer_no) {
          //load datas
          this.getInfo();

        } else {
          // this.clearData();
        }
      } catch (error) {
        console.log(error);
      }

    });

    this.client.on('connect', () => {
      console.log(`Connected!`);
      that.zone.run(() => {
        that.isOffline = false;
      });

      that.client.subscribe(topic, { qos: 0 }, (error) => {
        if (error) {
          that.zone.run(() => {
            that.isOffline = true;
            try {
              // that.counter.restart();
            } catch (error) {
              console.log(error);
            }
          });
        } else {
          console.log(`subscribe ${topic}`);
        }
      });
    });

    this.client.on('close', () => {
      console.log('MQTT Conection Close');
    });

    this.client.on('error', (error) => {
      console.log('MQTT Error');
      that.zone.run(() => {
        that.isOffline = true;
        // that.counter.restart();
      });
    });

    this.client.on('offline', () => {
      console.log('MQTT Offline');
      that.zone.run(() => {
        that.isOffline = true;
        try {
          // that.counter.restart();
        } catch (error) {
          console.log(error);
        }
      });
    });
  }
}
