import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferoutViewsOnlyComponent } from './referout-views-only.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referoutViewsOnlyRoutes: Route[] = [
  {
      path     : '',
      component: ReferoutViewsOnlyComponent
  }
];

@NgModule({
  declarations: [
    ReferoutViewsOnlyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referoutViewsOnlyRoutes)

  ]
})
export class ReferoutViewsOnlyModule { }
