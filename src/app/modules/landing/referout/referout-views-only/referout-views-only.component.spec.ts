import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferoutViewsOnlyComponent } from './referout-views-only.component';

describe('ReferoutViewsOnlyComponent', () => {
  let component: ReferoutViewsOnlyComponent;
  let fixture: ComponentFixture<ReferoutViewsOnlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferoutViewsOnlyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferoutViewsOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
