import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferoutViewsComponent } from './referout-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referoutViewsRoutes: Route[] = [
  {
      path     : '',
      component: ReferoutViewsComponent
  }
];

@NgModule({
  declarations: [
    ReferoutViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referoutViewsRoutes)

  ]
})
export class ReferoutViewsModule { }
