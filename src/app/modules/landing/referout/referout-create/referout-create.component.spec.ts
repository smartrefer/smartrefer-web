import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferoutCreateComponent } from './referout-create.component';

describe('ReferoutCreateComponent', () => {
  let component: ReferoutCreateComponent;
  let fixture: ComponentFixture<ReferoutCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferoutCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferoutCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
