import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferoutCreateComponent } from './referout-create.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referoutCreateRoutes: Route[] = [
  {
      path     : '',
      component: ReferoutCreateComponent
  }
];

@NgModule({
  declarations: [
    ReferoutCreateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referoutCreateRoutes)

  ]
})
export class ReferoutCreateModule { }
