import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferbackViewsOnlyComponent } from './referback-views-only.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referbackViewsOnlyRoutes: Route[] = [
  {
      path     : '',
      component: ReferbackViewsOnlyComponent
  }
];

@NgModule({
  declarations: [
    ReferbackViewsOnlyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referbackViewsOnlyRoutes)

  ]
})
export class ReferbackViewsOnlyModule { }
