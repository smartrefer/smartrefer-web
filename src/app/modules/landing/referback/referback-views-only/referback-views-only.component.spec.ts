import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferbackViewsOnlyComponent } from './referback-views-only.component';

describe('ReferbackViewsOnlyComponent', () => {
  let component: ReferbackViewsOnlyComponent;
  let fixture: ComponentFixture<ReferbackViewsOnlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferbackViewsOnlyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferbackViewsOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
