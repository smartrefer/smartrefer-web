import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferbackCreateComponent } from './referback-create.component';

describe('ReferbackCreateComponent', () => {
  let component: ReferbackCreateComponent;
  let fixture: ComponentFixture<ReferbackCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferbackCreateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferbackCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
