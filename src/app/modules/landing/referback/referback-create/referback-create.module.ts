import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferbackCreateComponent } from './referback-create.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referbackCreateRoutes: Route[] = [
  {
      path     : '',
      component: ReferbackCreateComponent
  }
];



@NgModule({
  declarations: [
    ReferbackCreateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referbackCreateRoutes)

  ]
})
export class ReferbackCreateModule { }
