import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferbackComponent } from './referback.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referbackRoutes: Route[] = [
  {
      path     : '',
      component: ReferbackComponent
  }
];

@NgModule({
  declarations: [
    ReferbackComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referbackRoutes)

  ]
})
export class ReferbackModule { }
