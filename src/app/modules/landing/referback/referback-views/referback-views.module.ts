import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferbackViewsComponent } from './referback-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const referbackViewsRoutes: Route[] = [
  {
      path     : '',
      component: ReferbackViewsComponent
  }
];

@NgModule({
  declarations: [
    ReferbackViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(referbackViewsRoutes)

  ]
})
export class ReferbackViewsModule { }
