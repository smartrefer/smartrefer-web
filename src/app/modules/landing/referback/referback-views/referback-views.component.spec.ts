import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferbackViewsComponent } from './referback-views.component';

describe('ReferbackViewsComponent', () => {
  let component: ReferbackViewsComponent;
  let fixture: ComponentFixture<ReferbackViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferbackViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferbackViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
