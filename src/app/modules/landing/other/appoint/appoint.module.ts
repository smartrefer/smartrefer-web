import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointComponent } from './appoint.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const appointRoutes: Route[] = [
  {
      path     : '',
      component: AppointComponent
  }
];

@NgModule({
  declarations: [
    AppointComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(appointRoutes)

  ]
})
export class AppointModule { }
