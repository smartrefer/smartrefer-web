import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssessComponent } from './assess.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const assessRoutes: Route[] = [
  {
      path     : '',
      component: AssessComponent
  }
];


@NgModule({
  declarations: [
    AssessComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(assessRoutes)
  ]
})
export class AssessModule { }
