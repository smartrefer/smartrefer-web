import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessViewsComponent } from './assess-views.component';

describe('AssessViewsComponent', () => {
  let component: AssessViewsComponent;
  let fixture: ComponentFixture<AssessViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssessViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AssessViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
