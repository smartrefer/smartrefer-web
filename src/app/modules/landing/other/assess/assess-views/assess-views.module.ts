import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssessViewsComponent } from './assess-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const assessViewsRoutes: Route[] = [
  {
      path     : '',
      component: AssessViewsComponent
  }
];

@NgModule({
  declarations: [
    AssessViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(assessViewsRoutes)
  ]
})
export class AssessViewsModule { }
