import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemedComponent } from './telemed.component';

describe('TelemedComponent', () => {
  let component: TelemedComponent;
  let fixture: ComponentFixture<TelemedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelemedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TelemedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
