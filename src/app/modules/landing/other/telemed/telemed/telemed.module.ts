import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelemedComponent } from './telemed.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const telemedRoutes: Route[] = [
  {
      path     : '',
      component: TelemedComponent
  }
];


@NgModule({
  declarations: [
    TelemedComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(telemedRoutes)
  ]
})
export class TelemedModule { }
