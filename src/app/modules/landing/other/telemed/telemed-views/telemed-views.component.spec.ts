import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemedViewsComponent } from './telemed-views.component';

describe('TelemedViewsComponent', () => {
  let component: TelemedViewsComponent;
  let fixture: ComponentFixture<TelemedViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelemedViewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TelemedViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
