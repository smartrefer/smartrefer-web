import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelemedViewsComponent } from './telemed-views.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const telemedViewsRoutes: Route[] = [
  {
      path     : '',
      component: TelemedViewsComponent
  }
];


@NgModule({
  declarations: [
    TelemedViewsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(telemedViewsRoutes)
  ]
})
export class TelemedViewsModule { }
