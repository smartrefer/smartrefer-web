import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TelemedViewsOnlyComponent } from './telemed-views-only.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const telemedViewsOnlyRoutes: Route[] = [
  {
      path     : '',
      component: TelemedViewsOnlyComponent
  }
];



@NgModule({
  declarations: [
    TelemedViewsOnlyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(telemedViewsOnlyRoutes)
  ]
})
export class TelemedViewsOnlyModule { }
