import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EvaluateComponent } from './evaluate.component';
import { Route, RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

const evaluateRoutes: Route[] = [
  {
      path     : '',
      component: EvaluateComponent
  }
];


@NgModule({
  declarations: [
    EvaluateComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(evaluateRoutes)
  ]
})
export class EvaluateModule { }
