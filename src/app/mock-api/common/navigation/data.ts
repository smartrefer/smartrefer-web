/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id   : 'referout',
        title: 'Rerfer Out',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-right',
        link : '/example'
    }, 
    {
        id   : 'referin',
        title: 'Rerfer In',
        type : 'basic',
        icon : 'heroicons_outline:chevron-right',
        link : '/example'
    }, 
    {
        id   : 'referback',
        title: 'Rerfer Back',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-left',
        link : '/example'
    },
    {
        id   : 'referreceive',
        title: 'Rerfer Receive',
        type : 'basic',
        icon : 'heroicons_outline:chevron-left',
        link : '/example'
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'referout',
        title: 'Rerfer Out',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-right',
        link : '/example'
    }, 
    {
        id   : 'referin',
        title: 'Rerfer In',
        type : 'basic',
        icon : 'heroicons_outline:chevron-right',
        link : '/example'
    },
    {
        id   : 'referback',
        title: 'Rerfer Back',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-left',
        link : '/example'
    },
    {
        id   : 'referreceive',
        title: 'Rerfer Receive',
        type : 'basic',
        icon : 'heroicons_outline:chevron-left',
        link : '/example'
    },
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'referout',
        title: 'Rerfer Out',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-right',
        link : '/example'
    }, 
    {
        id   : 'referin',
        title: 'Rerfer In',
        type : 'basic',
        icon : 'heroicons_outline:chevron-right',
        link : '/example'
    },
    {
        id   : 'referback',
        title: 'Rerfer Back',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-left',
        link : '/example'
    },
    {
        id   : 'referreceive',
        title: 'Rerfer Receive',
        type : 'basic',
        icon : 'heroicons_outline:chevron-left',
        link : '/example'
    },
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'referout',
        title: 'Rerfer Out',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-right',
        link : '/referout'
    }, 
    {
        id   : 'referin',
        title: 'Rerfer In',
        type : 'basic',
        icon : 'heroicons_outline:chevron-right',
        link : '/referin'
    },
    {
        id   : 'referback',
        title: 'Rerfer Back',
        type : 'basic',
        icon : 'heroicons_outline:arrow-circle-left',
        link : '/referback'
    },
    {
        id   : 'referreceive',
        title: 'Rerfer Receive',
        type : 'basic',
        icon : 'heroicons_outline:chevron-left',
        link : '/referreceive'
    },
    {
        id   : 'other',
        title: 'Other',
        type : 'aside',
        children : [
            {
                id   : 'appoint',
                title: 'Appoint',
                type : 'basic',
                icon : 'heroicons_outline:calendar',
                link : '/appoint'
            },
            {
                id   : 'telemed',
                title: 'TeleMed',
                type : 'basic',
                icon : 'heroicons_outline:video-camera',
                link : '/telemed'
            },
            {
                id   : 'evaluate',
                title: 'Evaluate',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-list',
                link : '/evaluate'
            },
            {
                id   : 'register',
                title: 'ส่งเยียมบ้าน',
                type : 'basic',
                icon : 'heroicons_outline:location-marker',
                link : '/register'
            },
            {
                id   : 'coc',
                title: 'ติดตามเยียมบ้าน',
                type : 'basic',
                icon : 'heroicons_outline:clipboard-check',
                link : '/coc'
            },
        ],
    },
    {
        id   : 'report',
        title: 'Report',
        type : 'aside',
        children : [
            {
                id   : 'phr',
                title: 'Phr',
                type : 'basic',
                icon : 'heroicons_outline:users',
                link : '/phr'
            },
            {
                id   : 'imc',
                title: 'IMC',
                type : 'basic',
                icon : 'heroicons_outline:save',
                link : '/imc'
            },
            {
                id   : 'report',
                title: 'Report',
                type : 'basic',
                icon : 'heroicons_outline:document-text',
                link : '/report'
            },
            {
                id   : 'referboard',
                title: 'Referboard',
                type : 'basic',
                icon : 'heroicons_outline:globe-alt',
                link : '/referboard'
            },
            
        ],
    },
];
