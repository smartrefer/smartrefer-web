import { Injectable } from '@angular/core'
import { Subject, BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class GlobalVariablesService {
  public globalVar = 'Oll  value';
  public scrHeight = '100px';
  public scrWidth = '100px';
  public paramsChild = '';

  rawData = [];

  private list = new BehaviorSubject<string[]>([]);
  readonly list$ = this.list.asObservable();

  constructor() {}

  addNewList(list:never) {
    // console.log(list);
    this.rawData.push(list);
    this.list.next(this.rawData);
  }

  removeList(list:never) {
    this.rawData = this.rawData.filter(v => v !== list);
    this.list.next(this.rawData);
  }
}