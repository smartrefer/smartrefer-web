import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'itemname'
})

export class ItemFormatPipe implements PipeTransform {

    transform(value: string, format: string): string {

        let text_result : any = '';      
        if(format == 'age'){
            let myArray = value.split("-");
            text_result = parseInt(myArray[0])+" ปี "+parseInt(myArray[1])+" เดือน "+parseInt(myArray[2])+" วัน ";
        }else if(format == 'cid'){
            let myArray = value.split("");
            text_result= myArray[0]+"-"+myArray[1]+myArray[2]+myArray[3]+myArray[4]+"-"+myArray[5]+myArray[6]+myArray[7]+myArray[8]+myArray[9]+"-"+myArray[10]+myArray[11]+"-"+myArray[12];
        }else if(format == 'time'){
            text_result = value.substring(0, 5) + ' น.';
        } else {
            text_result =  '-';
        }
        return text_result;
    }
}

